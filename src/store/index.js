import { createStore } from 'vuex'

const store = createStore({
  state() {
    return {
      characters: [],
      activePlanet: null,
      loading: false,
    }
  },
  mutations: {
    setActivePlanet(state, payload) {
      state.activePlanet = payload;
    },
    setCharacters(state, payload) {
      state.characters = payload;
    },
    setLoading(state, payload) {
      state.loading = payload;
    },
  },
});

export default store; 