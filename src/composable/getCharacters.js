import { ref } from '@vue/reactivity';
import { useStore } from 'vuex';

const getCharacters = () => {

    const characters = ref([]);
    const error = ref(null);
    const next = ref(true);
    const store = useStore(); 

    const load = async (pageNumber) => {
        try {
            let data = await fetch(`https://swapi.dev/api/people?page=${pageNumber}`);
            store.commit('setLoading', true);
            
            if (data.status === 200) {
                let res = await data.json();
                
                // Checks if "next" is returned to see if there's more results
                next.value = await res.next ? true : false;

                let charactersData = await Promise.all(res.results.map(async character => {
                    // Gets planet infromation
                    let data = await fetch(character.homeworld); 

                    if(data.status === 200) {
                        let planet = await data.json();
                        return {
                            ...character,
                            planet,
                        };
                    }
                    else {
                        throw Error('Cannot get planet infromation!');
                    }
                    
                }));

                store.commit('setLoading', false);
                store.commit('setCharacters', charactersData);
            }
            else {
                throw Error('Cannot get characters! Please refresh and try again.');
            }
        }
        catch (err) {
            error.value = err.message;
        }
    }

    return { characters, error, next, load };

};

export default getCharacters;